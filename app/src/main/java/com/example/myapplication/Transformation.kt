package com.example.myapplication

import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.input.OffsetMapping
import androidx.compose.ui.text.input.TransformedText
import androidx.compose.ui.text.input.VisualTransformation

class Transformation(
    private val findRegex: String,
    private val replaceRegex: String
) : VisualTransformation {

    override fun filter(text: AnnotatedString): TransformedText {

        val input: String = text.text

        val output = input.replace(findRegex.toRegex(), replaceRegex)

        val offsetTranslator = object : OffsetMapping {
            override fun originalToTransformed(offset: Int) = offset + output.length - input.length
            override fun transformedToOriginal(offset: Int) = offset + input.length - output.length
        }

        return TransformedText(AnnotatedString(output), offsetTranslator)
    }

}