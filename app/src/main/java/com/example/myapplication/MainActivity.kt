package com.example.myapplication

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.Surface
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.text.input.TextFieldValue
import com.example.myapplication.ui.theme.MyApplicationTheme

class MainActivity : ComponentActivity() {

    private val findRegex = """(\d{3})(\d{3})(\d{3})(\d{2})"""

    private val replaceRegex = """$1.$2.$3-$4"""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApplicationTheme {
                Surface {
                    MaskedTextField(findRegex, replaceRegex)
                }
            }
        }
    }
}

@Composable
fun MaskedTextField(inputRegex: String, outputRegex: String) {
    val text = remember { mutableStateOf(TextFieldValue()) }

    TextField(
        value = text.value,
        onValueChange = { text.value = it },
        visualTransformation = Transformation(inputRegex, outputRegex),
    )
}